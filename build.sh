#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"

# IN
SRC_DIR="${ABS_PATH_DIR}/src"
XNG_CONFIG="${SRC_DIR}/.artifacts.env"

# TEMP
TMP=${ABS_PATH_DIR}/tmp

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script gathers necessary artifacts and builds the docker image.
    This is meant to be called from the makefile.  If you call it manually, remember to supply the entire docker image name.
    e.g. gcr.io/xand-dev/xng-set:0.2.0

    Arguments:  Any argument is treated as a docker tag to build

    Input Variables:
      ARTIFACT_USER=value       Artifactory Username (required)
      ARTIFACT_PASS=value       Artifactory Password (required)
END
  )"
  echo -e "$HELPTEXT"
}

# Print help on -h
while getopts h opt
do
    case "${opt}" in
        h) print_help && exit ;;
    esac
done

create_temp_dir() {
  delete_temp_dir
  mkdir -p ${TMP}
}

copy_files_to_temp() {
    cp -r ${SRC_DIR}/. ${TMP}
}

load_vars() {
    export CARGO_REGISTRIES_TPFS_INDEX="ssh://git@gitlab.com/TransparentIncDevelopment/internaltools/tpfs-crates-index.git"
    source ${XNG_CONFIG}
}

get_xng() {
    cargo install xand_network_generator --registry tpfs --version ${XAND_NETWORK_GENERATOR_VERSION} -f --root ${TMP}
}

get_chainspec() {
  zip_filename="chain-spec-template.${CHAINSPEC_VERSION}.zip"

  wget --directory-prefix=${TMP} --user=${ARTIFACT_USER}  --password=${ARTIFACT_PASS} -O "${TMP}/chainspec.zip" \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${zip_filename}
}

build_docker() {
    for i in "$@"; do
      echo "Building ${i}"
	    docker build -t ${i} ${TMP}
    done
}

delete_temp_dir() {
    rm -rf ${TMP}
}

load_vars
create_temp_dir
copy_files_to_temp
get_xng
get_chainspec
build_docker $@
delete_temp_dir