#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"

# IN
CONFIG_TEMPLATE="${ABS_PATH_DIR}/config.template.yaml"
DEFAULTS_CONFIG="${ABS_PATH_DIR}/.defaults.env"
IMAGES_CONFIG="${ABS_PATH_DIR}/.images.env"
ARTIFACTS_CONFIG="${ABS_PATH_DIR}/.artifacts.env"

# OUT
OUT_DIR=${ABS_PATH_DIR}
OUT_NAME="generated"
RECEIPT_DIR_NAME="generated-receipt"
OUT=${OUT_DIR}/${OUT_NAME}
RECEIPT_OUT=${OUT}/${RECEIPT_DIR_NAME}
RECEIPT_OUT_CONFIG="${RECEIPT_OUT}/generated.config.yaml"
RECEIPT_OUT_ARTIFACTS="${RECEIPT_OUT}/used-artifacts.txt"

# TEMP
TMP="${ABS_PATH_DIR}/tmp"

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script generates a network via XNG using variables listed in .env and the configuration template config.template.yaml.
    To override a default setting, set an environment variable of that setting without "DEFAULT_" in the name.  See
    .defaults.env for a list of default settings.
END
  )"
  echo -e "$HELPTEXT"
}

# Print help on -h
while getopts h opt
do
    case "${opt}" in
        h) print_help && exit ;;
    esac
done

clean_build_dir() {
  rm -rf ${OUT}
}

create_temp_dir() {
  mkdir -p ${TMP}
}

load_vars() {
  source ${DEFAULTS_CONFIG}
  source ${IMAGES_CONFIG}
  source ${ARTIFACTS_CONFIG}
}

generate_config() {
  cp ${CONFIG_TEMPLATE} ${TMP}/config.yaml

  declare -A replacements

  replacements[MEMBER_API_IMAGE]=${MEMBER_API_IMAGE}
  replacements[VALIDATOR_IMAGE]=${VALIDATOR_IMAGE}
  replacements[TRUST_IMAGE]=${TRUST_IMAGE}
  replacements[BANK_MOCKS_IMAGE]=${BANK_MOCKS_IMAGE}

  replacements[CONFIDENTIAL_MODE]=${CONFIDENTIAL_MODE:-$DEFAULT_CONFIDENTIAL_MODE}
  replacements[VALIDATOR_COUNT]=${VALIDATOR_COUNT:-$DEFAULT_VALIDATOR_COUNT}
  replacements[MEMBER_COUNT]=${MEMBER_COUNT:-$DEFAULT_MEMBER_COUNT}
  replacements[ENABLE_AUTH]=${ENABLE_AUTH:-$DEFAULT_ENABLE_AUTH}

  echo -e "Creating XNG Config"
  for k in "${!replacements[@]}"
    do
        echo -e "\tReplacing ${k} with ${replacements[${k}]}"
        sed -i -e "s@\${${k}}@${replacements[${k}]}@" ${TMP}/config.yaml
    done
}

generate_xandbox_network() {
  xand_network_generator generate  \
    --config ${TMP}/config.yaml \
    --chainspec-zip  ${ABS_PATH_DIR}/"chainspec.zip" \
    --output-dir ${OUT}
}

generate_receipt() {
  mkdir -p ${RECEIPT_OUT}
  cp ${TMP}/config.yaml ${RECEIPT_OUT_CONFIG}
  echo "XNG Version: ${XAND_NETWORK_GENERATOR_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
  echo "Chainspec Version: ${CHAINSPEC_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
}

delete_temp_dir() {
    rm -rf ${TMP}
}

clean_build_dir
create_temp_dir
load_vars
generate_config
generate_xandbox_network
generate_receipt
delete_temp_dir
